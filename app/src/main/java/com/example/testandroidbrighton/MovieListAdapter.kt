package com.example.testandroidbrighton

import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.testandroidbrighton.model.list_movie.Search
import kotlinx.android.synthetic.main.listing_movie.view.*
import java.util.ArrayList

class MovieListAdapter (private val listItems: ArrayList<Search>,
) : RecyclerView.Adapter<MovieListAdapter.AdapterViewHolder>(){

    companion object{
        private val TAG = "MoviewListAdapter"
    }

    inner class AdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        @RequiresApi(Build.VERSION_CODES.N)
        fun bind(postResponse: Search){
            with(itemView){

                Glide.with(context).load(postResponse.Poster)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform().into(itemView.img_movie)

                itemView.t_judul.text = postResponse.Title
                itemView.t_tahun.text = postResponse.Year

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.listing_movie,
            parent,
            false
        )
        return AdapterViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {
        holder.bind(listItems[position])


        holder.itemView.setOnClickListener {
            val i = Intent(holder.itemView.context,DetailMovieActivity::class.java)
            i.putExtra("i",listItems[position].imdbID)
            holder.itemView.context.startActivity(i)
        }

    }

    override fun getItemCount(): Int = listItems.size




}

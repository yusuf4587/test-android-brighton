package com.example.testandroidbrighton.model.detail_movie

data class Rating(
    val Source: String,
    val Value: String
)
package com.example.testandroidbrighton.model.list_movie

data class MovieListModel(
    val Response: String,
    val Search: List<Search>,
    val totalResults: String
)
package com.example.testandroidbrighton

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.testandroidbrighton.db.MoveFavoriteModel
import com.example.testandroidbrighton.db.MovieListRoom
import com.example.testandroidbrighton.model.list_movie.Search
import kotlinx.android.synthetic.main.activity_movie_favorite.*

class MovieFavoriteActivity : AppCompatActivity() {

    lateinit var listItems :ArrayList<MoveFavoriteModel>
    lateinit var mlistingAdapter: MovieFavoriteAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_favorite)


        getAllData()
        swipe_container2.isRefreshing = true

        swipe_container2.setOnRefreshListener {
            listItems.clear()
            getAllData()
        }
    }

    override fun onResume() {
        super.onResume()
        listItems.clear()
        getAllData()
    }

    private fun getAllData(){
        val database = MovieListRoom.getDatabase(applicationContext)
        val dao = database.getMovielist()
        listItems = ArrayList<MoveFavoriteModel>()
        listItems.clear()
        listItems.addAll(dao.getAll())
        setupRecyclerview(rv_movie2)

        if(listItems.size>0){
            swipe_container2.isRefreshing = false
            text_view_note_empty2.visibility = View.GONE
        }else{
            text_view_note_empty2.visibility = View.VISIBLE
        }
    }

    private fun setupRecyclerview(recyclerView: RecyclerView) {
        try {
            val manager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            recyclerView.layoutManager = manager
            mlistingAdapter = MovieFavoriteAdapter(listItems)
            recyclerView.adapter = mlistingAdapter
        }catch (e: Exception){ }
    }


}
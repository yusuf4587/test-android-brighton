package com.example.testandroidbrighton.api

import com.example.testandroidbrighton.model.detail_movie.MovieDetailModel
import com.example.testandroidbrighton.model.list_movie.MovieListModel
import retrofit2.Call
import retrofit2.http.*

interface Api {

    @GET()// url
    fun getMovie(@Url url : String): Call<MovieListModel>


    @GET()// url
    fun getDetail(@Url url : String): Call<MovieDetailModel>

//    ?s=marvel&apikey=4e1c94e3&page=3



}
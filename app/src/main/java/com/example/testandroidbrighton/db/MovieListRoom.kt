package com.example.testandroidbrighton.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [MoveFavoriteModel::class],version = 1,exportSchema = false)
abstract class MovieListRoom: RoomDatabase() {

    companion object{
        @Volatile
        private var INSTANCE:MovieListRoom? = null

        fun getDatabase(context: Context): MovieListRoom{
            return INSTANCE ?: synchronized(this){
                // create database
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MovieListRoom::class.java,
                    "MovieList_db"
                )
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build()



                INSTANCE = instance
                instance

            }
        }
    }

    abstract fun getMovielist():MovieListDao
}
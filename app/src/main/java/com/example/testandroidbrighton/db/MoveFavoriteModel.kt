package com.example.testandroidbrighton.db

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "MovieListFavorite")

@Parcelize
data class MoveFavoriteModel(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")var id :Int= 0,
    @ColumnInfo(name = "Title")var Title :String= "",
    @ColumnInfo(name = "Year")var Year :String= "",
    @ColumnInfo(name = "imdbID")var imdbID :String= "",
    @ColumnInfo(name = "Type")var Type :String= "",
    @ColumnInfo(name = "Poster")var Poster :String= "",
    ): Parcelable{

    }

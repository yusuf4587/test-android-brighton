package com.example.testandroidbrighton.db

import androidx.room.*

@Dao
interface MovieListDao {

    @Insert
    fun insert(MovieListFavorite: MoveFavoriteModel)

//    @Update
//    fun update(MovieListFavorite:MoveFavoriteModel)

//    @Delete
//    fun delete(MovieListFavorite: MoveFavoriteModel)

    @Query("SELECT * FROM MovieListFavorite")
    fun getAll() : List<MoveFavoriteModel>

    @Query("SELECT * FROM MovieListFavorite where imdbID = :imdbid")
    fun getById(imdbid:String):List<MoveFavoriteModel>


    @Query("DELETE  FROM MovieListFavorite where imdbID = :imdbid")
    fun deteleById(imdbid:String)



}
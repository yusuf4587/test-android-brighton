package com.example.testandroidbrighton

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.testandroidbrighton.api.Client
import com.example.testandroidbrighton.model.list_movie.MovieListModel
import com.example.testandroidbrighton.model.list_movie.Search
import kotlinx.android.synthetic.main.movie_list_activity.*
import retrofit2.Call
import retrofit2.Response


class MovieListActivity : AppCompatActivity() {

    private var first = "page=1"
    private var loadNext = ""
    private var search_key = "marvel"

    private lateinit var listItems: ArrayList<Search>
    private lateinit var mlistingAdapter : MovieListAdapter

    private var loadMore = false    // First load

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.movie_list_activity)


        swipe_container.isRefreshing = true
        rvListener()
        rvList(rv_movie)
        loadMovie(first)

        swipe_container.setOnRefreshListener {
            listItems.clear()
            mlistingAdapter.notifyDataSetChanged()
            loadNext=""
            search_key = "marvel"
            loadMovie(first)
        }

        t_search.doAfterTextChanged {
            if (t_search.text.isNotEmpty()){
                search_key = t_search.text.toString()
                first = "page=1"
                Handler().postDelayed({
                    listItems.clear()
                    mlistingAdapter.notifyDataSetChanged()
                    loadMovie(page = first)
                },200)
            }else{
                listItems.clear()
                mlistingAdapter.notifyDataSetChanged()
                loadNext=""
                search_key = "marvel"
                loadMovie(first)
            }

        }

        favorit.setOnClickListener {
            startActivity(Intent(this,MovieFavoriteActivity::class.java))
        }





    }

    private fun loadMovie(page: String) {
        if(rv_movie!=null){
            Client.instance.getMovie("?s=$search_key&apikey=4e1c94e3&$page").enqueue(object :
                retrofit2.Callback<MovieListModel> {
                @RequiresApi(Build.VERSION_CODES.KITKAT)
                override fun onResponse(
                    call: Call<MovieListModel>,
                    response: Response<MovieListModel>
                ) {

                    Log.d("uch", "response ${response}")
                    loadMore = false
                    if (swipe_container != null) {
                        swipe_container.isRefreshing = false
                    }
                    try {
                        progressBar.visibility = View.INVISIBLE

                        listItems.addAll(response.body()!!.Search)
                        mlistingAdapter.notifyItemInserted(listItems.size)

                    } catch (e: Exception) {

                    }
                }

                override fun onFailure(call: Call<MovieListModel>, t: Throwable) {
                    try {
                        loadMore = false
                        progressBar.visibility = View.INVISIBLE

                        Log.d("uch", "response Throwable loadMovie -- $t")
                    } catch (e: java.lang.Exception) {
                    }

                }
            })
        }
    }


    private fun rvList(recyclerView: RecyclerView) {
        try {
            listItems = ArrayList<Search>()
            val manager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            recyclerView.layoutManager = manager
            mlistingAdapter = MovieListAdapter(listItems)
            recyclerView.adapter = mlistingAdapter
        }catch (e: Exception){ }
    }

    @SuppressLint("ResourceAsColor")
    private fun rvListener() {

        try {
            rv_movie.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (dy > 0) { //check for scroll down
                        val layoutManager = recyclerView.layoutManager as LinearLayoutManager?

                        val lastVisiblePosition: Int = layoutManager!!.findLastVisibleItemPosition()
                        if (lastVisiblePosition == recyclerView.childCount) {
                            if (!loadMore) {
                                loadMore = true
                                if (progressBar != null) {
                                    progressBar.visibility = View.VISIBLE
                                }
                                Handler().run {
                                    loadMovie(loadNext)
                                }                            }
                        }

                    }
                }
            })
        }catch (E: Exception){}
    }
}
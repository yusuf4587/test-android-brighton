package com.example.testandroidbrighton

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.testandroidbrighton.api.Client
import com.example.testandroidbrighton.db.MoveFavoriteModel
import com.example.testandroidbrighton.db.MovieListDao
import com.example.testandroidbrighton.db.MovieListRoom
import com.example.testandroidbrighton.model.detail_movie.MovieDetailModel
import kotlinx.android.synthetic.main.activity_detail_movie.*
import retrofit2.Call
import retrofit2.Response

class DetailMovieActivity : AppCompatActivity() {

    private lateinit var database: MovieListRoom
    private lateinit var dao: MovieListDao


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)
        database = MovieListRoom.getDatabase(applicationContext)
        dao = database.getMovielist()


        swipe_container.isRefreshing = true
        var i = ""
        i= intent.getStringExtra("i").toString()
        loadDetailMoview(i)


        swipe_container.setOnRefreshListener {
            loadDetailMoview(i)
        }


    }


    private fun loadDetailMoview(i:String) {
            Client.instance.getDetail("?i=$i&apikey=4e1c94e3").enqueue(object :
                retrofit2.Callback<MovieDetailModel> {
                @RequiresApi(Build.VERSION_CODES.KITKAT)
                override fun onResponse(
                    call: Call<MovieDetailModel>,
                    response: Response<MovieDetailModel>
                ) {

                    try {
                        swipe_container.isRefreshing = false
                        Log.d("uch","response detail moview ${response}")
                        val data = response.body()!!
                        Glide.with(this@DetailMovieActivity).load(data.Poster)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .dontTransform().into(img)


                        t_judul_film.text = data.Title
                        t_date.text = data.Released
                        t_runtime.text = data.Runtime
                        t_rating.text = data.Ratings[0].Value


                        t_desc.text = data.Plot

                        t_genre.text = data.Genre
                        t_director.text = data.Director
                        t_actors.text = data.Actors

                        favorite.setOnClickListener {
                            FavoriteMovie(
                                MoveFavoriteModel(
                                    Title = data.Title,
                                    Year = data.Year,
                                    imdbID = data.imdbID,
                                    Type =  data.Type,
                                    Poster = data.Poster,
                                )
                            )
                        }



                    } catch (e: Exception) { }
                }

                override fun onFailure(call: Call<MovieDetailModel>, t: Throwable) {
                    try {
                        swipe_container.isRefreshing = false
                        Log.d("uch", "response Throwable detail  -- $t")
                    } catch (e: java.lang.Exception) { }

                }
            })

    }


    private fun FavoriteMovie(movie: MoveFavoriteModel) {

        if (dao.getById(movie.imdbID).isEmpty()) {
            dao.insert(movie)
            Toast.makeText(applicationContext, "Berhasil Ditambahkan", Toast.LENGTH_SHORT).show()
        } else {
            dao.deteleById(movie.imdbID)
            Toast.makeText(applicationContext, "Berhasil Dihapus", Toast.LENGTH_SHORT).show()
        }

    }
}